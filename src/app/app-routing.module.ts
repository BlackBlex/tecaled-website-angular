import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    {
        path: 'home',
        loadChildren: () =>
            import('./pages/home/home.module').then((m) => m.HomeModule),
    },
    {
        path: 'not-found',
        loadChildren: () =>
            import('./pages/not-found/not-found.module').then(
                (m) => m.NotFoundModule
            ),
    },
    {
        path: 'stories',
        loadChildren: () =>
            import('./pages/stories/stories.module').then(
                (m) => m.StoriesModule
            ),
    },
    {
        path: 'services',
        loadChildren: () =>
            import('./pages/services/services.module').then(
                (m) => m.ServicesModule
            ),
    },
    {
        path: 'contact',
        loadChildren: () =>
            import('./pages/contact/contact.module').then(
                (m) => m.ContactModule
            ),
    },
    {
        path: 'distributor',
        loadChildren: () =>
            import('./pages/distributor/distributor.module').then(
                (m) => m.DistributorModule
            ),
    },
    { path: '**', redirectTo: 'not-found' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
