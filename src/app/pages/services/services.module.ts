import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from './services.component';
import { ConsultingComponent } from './consulting/consulting.component';
import { LteComponent } from './lte/lte.component';
import { RadioPlanningComponent } from './radio-planning/radio-planning.component';
import { SatelliteComponent } from './satellite/satellite.component';
import { TechnicalSupportComponent } from './technical-support/technical-support.component';

import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NavigationServicesComponent } from '../../components/navigation-services/navigation-services.component';

@NgModule({
    declarations: [
        ServicesComponent,
        ConsultingComponent,
        LteComponent,
        RadioPlanningComponent,
        SatelliteComponent,
        TechnicalSupportComponent,
        NavigationServicesComponent,
    ],
    imports: [CommonModule, ServicesRoutingModule, SlickCarouselModule],
})
export class ServicesModule {}
