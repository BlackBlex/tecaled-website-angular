import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

const enterTransition = transition(':enter', [
    style({
        opacity: 0,
    }),
    animate(
        '300ms ease-in',
        style({
            opacity: 1,
        })
    ),
]);

const leaveTrans = transition(':leave', [
    style({
        opacity: 1,
    }),
    animate(
        '300ms ease-out',
        style({
            opacity: 0,
        })
    ),
]);

const fadeIn = trigger('fadeIn', [enterTransition]);

const fadeOut = trigger('fadeOut', [leaveTrans]);

@Component({
    selector: 'app-consulting',
    templateUrl: './consulting.component.html',
    styleUrls: ['./consulting.component.css'],
    animations: [fadeIn, fadeOut],
})
export class ConsultingComponent implements OnInit {
    constructor() {}

    show_first: boolean = true;

    ngOnInit(): void {}

    onClickLeft(): void {
        this.show_first = true;
        this.scrollUp();
    }

    onClickRight(): void {
        this.show_first = false;
        this.scrollUp();
    }

    scrollUp(): void {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth',
        });
    }
}
