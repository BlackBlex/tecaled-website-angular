import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioPlanningComponent } from './radio-planning.component';

describe('RadioPlanningComponent', () => {
    let component: RadioPlanningComponent;
    let fixture: ComponentFixture<RadioPlanningComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [RadioPlanningComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RadioPlanningComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
