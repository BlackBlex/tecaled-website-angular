import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-lte',
    templateUrl: './lte.component.html',
    styleUrls: ['./lte.component.css'],
})
export class LteComponent implements OnInit {
    slides: Array<Array<String>> = [
        [
            'assets/images/lte/lte.png',
            'LTE',
            'Convivencia, interoperabilidad, eficiencia, resiliencia',
        ],
        [
            'assets/images/lte/proveedor.png',
            'PROVEEDOR ÚNICO',
            'Solución fin a fin',
        ],
        [
            'assets/images/lte/banda.png',
            'BANDAS DE FRECUENCIA',
            '2.5GHz (Bandas 40.41), 5GHZ (Banda 46)',
        ],
        [
            'assets/images/lte/construccion.png',
            'CONSTRUIDO ESPECIALMENTE',
            'Carrier class, NLOS, programador inteligente, servicio de capa 2 y todo al aire libre',
        ],
        [
            'assets/images/lte/espectro.png',
            'ESPECTRO FLEXIBLE',
            'Agregación de portadora interbanda e intrabanda no contigua',
        ],
        [
            'assets/images/lte/servicios.png',
            'SERVICIOS',
            'Arquitectura de red flexible con EPC integrado o externo',
        ],
        [
            'assets/images/lte/tco.png',
            'TCO & ROI',
            'Múltiples topologías de implementación, LTE dentro de una caja, paga a medida que creces',
        ],
        [
            'assets/images/lte/software.png',
            'ECOSISTEMA',
            'Software unificado para gestión y facturación',
        ],
    ];

    slideConfig = {
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        pauseOnFocus: false,
        pauseOnHover: false,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 6,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                },
            },
        ],
    };

    slideConfig2 = {
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1500,
        autoplay: true,
        arrows: true,
        pauseOnFocus: true,
        pauseOnHover: true,
    };

    constructor() {}

    ngOnInit(): void {}
}
