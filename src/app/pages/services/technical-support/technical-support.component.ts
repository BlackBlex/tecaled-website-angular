import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from 'src/app/common/global-constants';

@Component({
    selector: 'app-technical-support',
    templateUrl: './technical-support.component.html',
    styleUrls: ['./technical-support.component.css'],
})
export class TechnicalSupportComponent implements OnInit {
    waUrl: string = GlobalConstants.waUrl;

    constructor() {}

    ngOnInit(): void {}
}
