import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServicesComponent } from './services.component';

import { ConsultingComponent } from './consulting/consulting.component';
import { LteComponent } from './lte/lte.component';
import { RadioPlanningComponent } from './radio-planning/radio-planning.component';
import { SatelliteComponent } from './satellite/satellite.component';
import { TechnicalSupportComponent } from './technical-support/technical-support.component';

const routes: Routes = [
    {
        path: '',
        component: ServicesComponent,
        children: [
            { path: '', redirectTo: 'consulting', pathMatch: 'full' },
            {
                path: 'consulting',
                component: ConsultingComponent,
                data: { animationState: 'consulting' },
            },
            {
                path: 'lte',
                component: LteComponent,
                data: { animationState: 'lte' },
            },
            {
                path: 'radio-planning',
                component: RadioPlanningComponent,
                data: { animationState: 'radio' },
            },
            {
                path: 'satellite',
                component: SatelliteComponent,
                data: { animationState: 'satellite' },
            },
            {
                path: 'technical-support',
                component: TechnicalSupportComponent,
                data: { animationState: 'technical' },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ServicesRoutingModule {}
