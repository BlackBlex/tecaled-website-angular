import {
    animate,
    animateChild,
    group,
    query,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.css'],
    animations: [
        trigger('slider_page', [
            transition(
                'consulting => radio, consulting => satellite, consulting => lte, consulting => technical, radio => satellite, radio => lte, radio => technical, satellite => lte, satellite => technical, lte => technical',
                [
                    style({ position: 'relative' }),
                    query(
                        ':enter, :leave',
                        style({
                            position: 'absolute',
                            width: '100%',
                            top: '48px',
                            right: 0,
                        }),
                        { optional: true }
                    ),
                    query(':enter', [
                        style({ transform: 'translateX(-100%)', opacity: 0 }),
                    ]),
                    query(':leave', animateChild()),
                    group([
                        query(
                            ':enter',
                            [
                                style({
                                    transform: 'translateX(100%)',
                                    opacity: 0,
                                }),
                                animate(
                                    '400ms ease-out',
                                    style({
                                        transform: 'translateX(0%)',
                                        opacity: 1,
                                    })
                                ),
                            ],
                            {
                                optional: true,
                            }
                        ),
                        query(
                            ':leave',
                            [
                                style({
                                    transform: 'translateX(0%)',
                                    opacity: 1,
                                }),
                                animate(
                                    '400ms ease-out',
                                    style({
                                        transform: 'translateX(-100%)',
                                        opacity: 0,
                                    })
                                ),
                            ],
                            {
                                optional: true,
                            }
                        ),
                    ]),
                    query(':enter', animateChild()),
                ]
            ),
            transition(
                'technical => lte, technical => satellite, technical => radio, technical => consulting, lte => satellite, lte => radio, lte => consulting, satellite => radio, satellite => consulting, radio => consulting',
                [
                    style({ position: 'relative' }),
                    query(
                        ':enter, :leave',
                        style({
                            position: 'absolute',
                            top: '48px',
                            left: 0,
                            width: '100%',
                        }),
                        { optional: true }
                    ),
                    query(':enter', [
                        style({ transform: 'translateX(100%)', opacity: 0 }),
                    ]),
                    query(':leave', animateChild()),
                    group([
                        query(
                            ':enter',
                            [
                                style({
                                    transform: 'translateX(-100%)',
                                    opacity: 0,
                                }),
                                animate(
                                    '400ms ease-out',
                                    style({
                                        transform: 'translateX(0%)',
                                        opacity: 1,
                                    })
                                ),
                            ],
                            {
                                optional: true,
                            }
                        ),
                        query(
                            ':leave',
                            [
                                style({
                                    transform: 'translateX(0%)',
                                    opacity: 1,
                                }),
                                animate(
                                    '400ms ease-out',
                                    style({
                                        transform: 'translateX(100%)',
                                        opacity: 0,
                                    })
                                ),
                            ],
                            {
                                optional: true,
                            }
                        ),
                    ]),
                    query(':enter', animateChild()),
                ]
            ),
        ]),
    ],
})
export class ServicesComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}

    prepareRoute(outlet: RouterOutlet): boolean {
        return (
            outlet &&
            outlet.activatedRouteData &&
            outlet.activatedRouteData['animationState']
        );
    }
}
