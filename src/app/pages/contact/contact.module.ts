import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
    RecaptchaFormsModule,
    RecaptchaModule,
    RecaptchaSettings,
    RECAPTCHA_SETTINGS,
} from 'ng-recaptcha';
import { environment } from 'src/environments/environment';
import { ContactFormComponent } from '../../components/contact-form/contact-form.component';

@NgModule({
    declarations: [ContactComponent, ContactFormComponent],
    imports: [
        CommonModule,
        ContactRoutingModule,
        ReactiveFormsModule,
        RecaptchaModule,
        RecaptchaFormsModule,
        HttpClientModule,
    ],
    providers: [
        {
            provide: RECAPTCHA_SETTINGS,
            useValue: {
                siteKey: environment.recaptcha.siteKey,
            } as RecaptchaSettings,
        },
    ],
})
export class ContactModule {}
