import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit {
    constructor() {}

    // BUG: aoskokas
    // CHANGE: aoskokas
    // DEBUG: aoskokas
    // DEPRECATED: aoskokas
    // ERROR: aoskokas
    // EXAMPLE: aoskokas
    // FIXME: aoskokas
    // IDEA: aoskokas
    // HACK: aoskokas
    // INFO: aoskokas
    // NOTE: aoskokas
    // OPTIMIZE: aoskokas
    // REFACTOR: aoskokas
    // REMOVE: aoskokas
    // HACK: aoskokas
    // REVIEW: aoskokas
    // TODO: aoskokas
    // WARNING: aoskokas

    ngOnInit(): void {}
}
