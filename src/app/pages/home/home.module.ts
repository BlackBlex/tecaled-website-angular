import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { AboutUsComponent } from '../../components/about-us/about-us.component';
import { OurClientsComponent } from '../../components/our-clients/our-clients.component';
import { OurServicesComponent } from '../../components/our-services/our-services.component';
import { WhyWeComponent } from '../../components/why-we/why-we.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
  declarations: [
    HomeComponent,
    AboutUsComponent,
    OurClientsComponent,
    OurServicesComponent,
    WhyWeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgbModule,
    SlickCarouselModule
  ]
})
export class HomeModule { }
