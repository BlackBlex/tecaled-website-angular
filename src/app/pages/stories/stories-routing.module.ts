import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoriesComponent } from './stories.component';

import { AkalCComponent } from './akal-c/akal-c.component';
import { EkAComponent } from './ek-a/ek-a.component';
import { KabCComponent } from './kab-c/kab-c.component';
import { KuFComponent } from './ku-f/ku-f.component';
import { KuHComponent } from './ku-h/ku-h.component';
import { IxtAComponent } from './ixt-a/ixt-a.component';
import { GalleryComponent } from './gallery/gallery.component';

const routes: Routes = [
    {
        path: '',
        component: StoriesComponent,
        children: [
            { path: '', redirectTo: 'gallery', pathMatch: 'full' },
            { path: 'gallery', component: GalleryComponent },
            { path: 'kabc', component: KabCComponent },
            { path: 'kuf', component: KuFComponent },
            { path: 'eka', component: EkAComponent },
            { path: 'akalc', component: AkalCComponent },
            { path: 'kuh', component: KuHComponent },
            { path: 'ixta', component: IxtAComponent },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class StoriesRoutingModule {}
