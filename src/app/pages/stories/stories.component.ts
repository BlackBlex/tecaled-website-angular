import { Component, OnInit } from '@angular/core';

import { Location } from '@angular/common';
import { NavigationEnd, Router } from '@angular/router';

@Component({
    selector: 'app-stories',
    templateUrl: './stories.component.html',
    styleUrls: ['./stories.component.css'],
})
export class StoriesComponent implements OnInit {
    isMain: boolean = true;

    constructor(private _location: Location, router: Router) {
        router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                if (!val.url.includes('gallery')) {
                    this.isMain = false;
                    this.scrollUp();
                } else {
                    this.isMain = true;
                }
            }
        });
    }

    ngOnInit(): void {}

    onClick() {
        this._location.back();
    }

    scrollUp(): void {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth',
        });
    }
}
