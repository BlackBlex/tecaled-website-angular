import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoriesRoutingModule } from './stories-routing.module';
import { StoriesComponent } from './stories.component';
import { AkalCComponent } from './akal-c/akal-c.component';
import { EkAComponent } from './ek-a/ek-a.component';
import { KabCComponent } from './kab-c/kab-c.component';
import { KuFComponent } from './ku-f/ku-f.component';
import { KuHComponent } from './ku-h/ku-h.component';
import { GalleryComponent } from './gallery/gallery.component';

import { GalleryModule } from 'ng-gallery';
import { LightboxModule } from 'ng-gallery/lightbox';
import { IxtAComponent } from './ixt-a/ixt-a.component';

@NgModule({
    declarations: [
        StoriesComponent,
        AkalCComponent,
        EkAComponent,
        KabCComponent,
        KuFComponent,
        KuHComponent,
        GalleryComponent,
        IxtAComponent,
    ],
    imports: [
        CommonModule,
        StoriesRoutingModule,
        GalleryModule.withConfig({
            thumb: false,
        }),
        LightboxModule,
    ],
})
export class StoriesModule {}
