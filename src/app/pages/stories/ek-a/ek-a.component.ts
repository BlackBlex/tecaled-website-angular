import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Gallery, GalleryItem, ImageItem, ImageSize } from 'ng-gallery';
import { Lightbox } from 'ng-gallery/lightbox';

@Component({
    selector: 'app-ek-a',
    templateUrl: './ek-a.component.html',
    styleUrls: ['./ek-a.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EkAComponent implements OnInit {
    items: GalleryItem[] = [];
    imageData = [
        { url: 'assets/images/stories/ATLAN/1.jpg' },
        { url: 'assets/images/stories/ATLAN/2.jpg' },
        { url: 'assets/images/stories/ATLAN/3.jpg' },
        { url: 'assets/images/stories/ATLAN/4.jpg' },
        { url: 'assets/images/stories/ATLAN/5.jpg' },
        { url: 'assets/images/stories/ATLAN/6.jpg' },
        { url: 'assets/images/stories/ATLAN/7.jpg' },
    ];

    constructor(public gallery: Gallery, public lightbox: Lightbox) {}

    ngOnInit(): void {
        // Creat gallery items
        this.items = this.imageData.map(
            (item) =>
                new ImageItem({
                    src: item.url,
                    thumb: item.url.replace('.jpg', '-sm.jpg'),
                })
        );

        /** Lightbox Example */

        // Get a lightbox gallery ref
        const lightboxRef = this.gallery.ref('lightbox');

        // Add custom gallery config to the lightbox (optional)
        lightboxRef.setConfig({
            imageSize: ImageSize.Contain,
        });

        // Load items into the lightbox gallery ref
        lightboxRef.load(this.items);
    }
}
