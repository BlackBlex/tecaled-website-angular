import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EkAComponent } from './ek-a.component';

describe('EkAComponent', () => {
    let component: EkAComponent;
    let fixture: ComponentFixture<EkAComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [EkAComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EkAComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
