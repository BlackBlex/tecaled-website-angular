import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Gallery, GalleryItem, ImageItem, ImageSize } from 'ng-gallery';
import { Lightbox } from 'ng-gallery/lightbox';

@Component({
    selector: 'app-ku-h',
    templateUrl: './ku-h.component.html',
    styleUrls: ['./ku-h.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KuHComponent implements OnInit {
    items: GalleryItem[] = [];
    imageData = [
        { url: 'assets/images/stories/IOLA/1.jpg' },
        { url: 'assets/images/stories/IOLA/2.jpg' },
        { url: 'assets/images/stories/IOLA/3.jpg' },
        { url: 'assets/images/stories/IOLA/4.jpg' },
        { url: 'assets/images/stories/IOLA/5.jpg' },
        { url: 'assets/images/stories/IOLA/6.jpg' },
        { url: 'assets/images/stories/IOLA/7.jpg' },
        { url: 'assets/images/stories/IOLA/8.jpg' },
        { url: 'assets/images/stories/IOLA/9.jpg' },
        { url: 'assets/images/stories/IOLA/10.jpg' },
        { url: 'assets/images/stories/IOLA/11.jpg' },
        { url: 'assets/images/stories/IOLA/12.jpg' },
        { url: 'assets/images/stories/IOLA/13.jpg' },
        { url: 'assets/images/stories/IOLA/14.jpg' },
        { url: 'assets/images/stories/IOLA/15.jpg' },
        { url: 'assets/images/stories/IOLA/16.jpg' },
        { url: 'assets/images/stories/IOLA/17.jpg' },
    ];

    constructor(public gallery: Gallery, public lightbox: Lightbox) {}

    ngOnInit(): void {
        // Creat gallery items
        this.items = this.imageData.map(
            (item) =>
                new ImageItem({
                    src: item.url,
                    thumb: item.url.replace('.jpg', '-sm.jpg'),
                })
        );

        /** Lightbox Example */

        // Get a lightbox gallery ref
        const lightboxRef = this.gallery.ref('lightbox');

        // Add custom gallery config to the lightbox (optional)
        lightboxRef.setConfig({
            imageSize: ImageSize.Contain,
        });

        // Load items into the lightbox gallery ref
        lightboxRef.load(this.items);
    }
}
