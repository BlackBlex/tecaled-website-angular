import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KuHComponent } from './ku-h.component';

describe('KuHComponent', () => {
    let component: KuHComponent;
    let fixture: ComponentFixture<KuHComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [KuHComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(KuHComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
