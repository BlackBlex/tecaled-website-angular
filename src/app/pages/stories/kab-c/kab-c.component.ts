import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Gallery, GalleryItem, ImageItem, ImageSize } from 'ng-gallery';
import { Lightbox } from 'ng-gallery/lightbox';

@Component({
    selector: 'app-kab-c',
    templateUrl: './kab-c.component.html',
    styleUrls: ['./kab-c.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KabCComponent implements OnInit {
    items: GalleryItem[] = [];
    imageData = [
        { url: 'assets/images/stories/KAB-C/1.jpg' },
        { url: 'assets/images/stories/KAB-C/2.jpg' },
        { url: 'assets/images/stories/KAB-C/3.jpg' },
        { url: 'assets/images/stories/KAB-C/4.jpg' },
        { url: 'assets/images/stories/KAB-C/5.jpg' },
        { url: 'assets/images/stories/KAB-C/6.jpg' },
        { url: 'assets/images/stories/KAB-C/7.jpg' },
        { url: 'assets/images/stories/KAB-C/8.jpg' },
        { url: 'assets/images/stories/KAB-C/9.jpg' },
        { url: 'assets/images/stories/KAB-C/10.jpg' },
        { url: 'assets/images/stories/KAB-C/11.jpg' },
        { url: 'assets/images/stories/KAB-C/12.jpg' },
        { url: 'assets/images/stories/KAB-C/13.jpg' },
        { url: 'assets/images/stories/KAB-C/14.jpg' },
        { url: 'assets/images/stories/KAB-C/15.jpg' },
        { url: 'assets/images/stories/KAB-C/16.jpg' },
        { url: 'assets/images/stories/KAB-C/17.jpg' },
    ];

    constructor(public gallery: Gallery, public lightbox: Lightbox) {}

    ngOnInit(): void {
        // Creat gallery items
        this.items = this.imageData.map(
            (item) =>
                new ImageItem({
                    src: item.url,
                    thumb: item.url.replace('.jpg', '-sm.jpg'),
                })
        );

        /** Lightbox Example */

        // Get a lightbox gallery ref
        const lightboxRef = this.gallery.ref('lightbox');

        // Add custom gallery config to the lightbox (optional)
        lightboxRef.setConfig({
            imageSize: ImageSize.Contain,
        });

        // Load items into the lightbox gallery ref
        lightboxRef.load(this.items);
    }
}
