import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KabCComponent } from './kab-c.component';

describe('KabCComponent', () => {
    let component: KabCComponent;
    let fixture: ComponentFixture<KabCComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [KabCComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(KabCComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
