import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Gallery, GalleryItem, ImageItem, ImageSize } from 'ng-gallery';
import { Lightbox } from 'ng-gallery/lightbox';

@Component({
    selector: 'app-akal-c',
    templateUrl: './akal-c.component.html',
    styleUrls: ['./akal-c.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AkalCComponent implements OnInit {
    items: GalleryItem[] = [];
    imageData = [
        { url: 'assets/images/stories/NEPT/1.jpg' },
        { url: 'assets/images/stories/NEPT/2.jpg' },
        { url: 'assets/images/stories/NEPT/3.jpg' },
        { url: 'assets/images/stories/NEPT/4.jpg' },
        { url: 'assets/images/stories/NEPT/5.jpg' },
        { url: 'assets/images/stories/NEPT/6.jpg' },
        { url: 'assets/images/stories/NEPT/7.jpg' },
        { url: 'assets/images/stories/NEPT/8.jpg' },
        { url: 'assets/images/stories/NEPT/9.jpg' },
        { url: 'assets/images/stories/NEPT/10.jpg' },
        { url: 'assets/images/stories/NEPT/11.jpg' },
        { url: 'assets/images/stories/NEPT/12.jpg' },
    ];

    constructor(public gallery: Gallery, public lightbox: Lightbox) {}

    ngOnInit(): void {
        // Creat gallery items
        this.items = this.imageData.map(
            (item) =>
                new ImageItem({
                    src: item.url,
                    thumb: item.url.replace('.jpg', '-sm.jpg'),
                })
        );

        /** Lightbox Example */

        // Get a lightbox gallery ref
        const lightboxRef = this.gallery.ref('lightbox');

        // Add custom gallery config to the lightbox (optional)
        lightboxRef.setConfig({
            imageSize: ImageSize.Contain,
        });

        // Load items into the lightbox gallery ref
        lightboxRef.load(this.items);
    }
}
