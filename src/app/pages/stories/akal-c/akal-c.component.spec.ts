import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AkalCComponent } from './akal-c.component';

describe('AkalCComponent', () => {
    let component: AkalCComponent;
    let fixture: ComponentFixture<AkalCComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AkalCComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AkalCComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
