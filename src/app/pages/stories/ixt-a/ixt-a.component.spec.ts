import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IxtAComponent } from './ixt-a.component';

describe('IxtAComponent', () => {
  let component: IxtAComponent;
  let fixture: ComponentFixture<IxtAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IxtAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IxtAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
