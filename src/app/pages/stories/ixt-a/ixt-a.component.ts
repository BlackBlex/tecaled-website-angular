import { Component, OnInit } from '@angular/core';

import { Gallery, GalleryItem, ImageItem, ImageSize } from 'ng-gallery';
import { Lightbox } from 'ng-gallery/lightbox';

@Component({
    selector: 'app-ixt-a',
    templateUrl: './ixt-a.component.html',
    styleUrls: ['./ixt-a.component.css'],
})
export class IxtAComponent implements OnInit {
    items: GalleryItem[] = [];
    imageData = [
        { url: 'assets/images/stories/IXT-A/1.jpg' },
        { url: 'assets/images/stories/IXT-A/2.jpg' },
        { url: 'assets/images/stories/IXT-A/3.jpg' },
        { url: 'assets/images/stories/IXT-A/4.jpg' },
        { url: 'assets/images/stories/IXT-A/5.jpg' },
        { url: 'assets/images/stories/IXT-A/6.jpg' },
        { url: 'assets/images/stories/IXT-A/7.jpg' },
        { url: 'assets/images/stories/IXT-A/8.jpg' },
        { url: 'assets/images/stories/IXT-A/9.jpg' },
        { url: 'assets/images/stories/IXT-A/10.jpg' },
        { url: 'assets/images/stories/IXT-A/11.jpg' },
        { url: 'assets/images/stories/IXT-A/12.jpg' },
        { url: 'assets/images/stories/IXT-A/13.jpg' },
        { url: 'assets/images/stories/IXT-A/14.jpg' },
        { url: 'assets/images/stories/IXT-A/15.jpg' },
    ];

    constructor(public gallery: Gallery, public lightbox: Lightbox) {}

    ngOnInit(): void {
        // Creat gallery items
        this.items = this.imageData.map(
            (item) =>
                new ImageItem({
                    src: item.url,
                    thumb: item.url.replace('.jpg', '-sm.jpg'),
                })
        );

        /** Lightbox Example */

        // Get a lightbox gallery ref
        const lightboxRef = this.gallery.ref('lightbox');

        // Add custom gallery config to the lightbox (optional)
        lightboxRef.setConfig({
            imageSize: ImageSize.Contain,
        });

        // Load items into the lightbox gallery ref
        lightboxRef.load(this.items);
    }
}
