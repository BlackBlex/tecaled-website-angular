import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Gallery, GalleryItem, ImageItem, ImageSize } from 'ng-gallery';
import { Lightbox } from 'ng-gallery/lightbox';

@Component({
    selector: 'app-ku-f',
    templateUrl: './ku-f.component.html',
    styleUrls: ['./ku-f.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KuFComponent implements OnInit {
    items: GalleryItem[] = [];
    imageData = [
        { url: 'assets/images/stories/KUF/1.jpg' },
        { url: 'assets/images/stories/KUF/2.jpg' },
        { url: 'assets/images/stories/KUF/3.jpg' },
        { url: 'assets/images/stories/KUF/4.jpg' },
        { url: 'assets/images/stories/KUF/5.jpg' },
        { url: 'assets/images/stories/KUF/6.jpg' },
        { url: 'assets/images/stories/KUF/7.jpg' },
        { url: 'assets/images/stories/KUF/8.jpg' },
    ];

    constructor(public gallery: Gallery, public lightbox: Lightbox) {}

    ngOnInit(): void {
        // Creat gallery items
        this.items = this.imageData.map(
            (item) =>
                new ImageItem({
                    src: item.url,
                    thumb: item.url.replace('.jpg', '-sm.jpg'),
                })
        );

        /** Lightbox Example */

        // Get a lightbox gallery ref
        const lightboxRef = this.gallery.ref('lightbox');

        // Add custom gallery config to the lightbox (optional)
        lightboxRef.setConfig({
            imageSize: ImageSize.Contain,
        });

        // Load items into the lightbox gallery ref
        lightboxRef.load(this.items);
    }
}
