import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KuFComponent } from './ku-f.component';

describe('KuFComponent', () => {
    let component: KuFComponent;
    let fixture: ComponentFixture<KuFComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [KuFComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(KuFComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
