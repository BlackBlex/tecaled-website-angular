import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistributorRoutingModule } from './distributor-routing.module';
import { DistributorComponent } from './distributor.component';
import { OurBrandsComponent } from '../../components/our-brands/our-brands.component';

import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
    declarations: [DistributorComponent, OurBrandsComponent],
    imports: [CommonModule, DistributorRoutingModule, SlickCarouselModule],
})
export class DistributorModule {}
