import { Component, OnInit } from '@angular/core';
import {
    transition,
    trigger,
    query,
    style,
    animate,
    group,
    animateChild,
} from '@angular/animations';

import AOS from 'aos';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    animations: [
        trigger('fade', [
            transition('* => *', [
                style({ position: 'relative' }),
                query(
                    ':enter, :leave',
                    style({
                        position: 'absolute',
                        width: '100%',
                        top: 0,
                        right: 0,
                    }),
                    { optional: true }
                ),
                query(':enter', [style({ opacity: 0 })], { optional: true }),
                query(':leave', animateChild(), { optional: true }),
                group([
                    query(
                        ':enter',
                        [
                            style({
                                opacity: 0,
                            }),
                            animate(
                                '600ms ease-out',
                                style({
                                    opacity: 1,
                                })
                            ),
                        ],
                        {
                            optional: true,
                        }
                    ),
                    query(
                        ':leave',
                        [
                            style({
                                opacity: 1,
                            }),
                            animate(
                                '600ms ease-out',
                                style({
                                    opacity: 0,
                                })
                            ),
                        ],
                        {
                            optional: true,
                        }
                    ),
                ]),
                query(':enter', animateChild(), { optional: true }),
            ]),
        ]),
    ],
})
export class AppComponent implements OnInit {
    title = 'Tecaled S.A de C.V';
    loader = true;

    constructor() {
        AOS.init();
    }

    ngOnInit(): void {
        //Loader variable set false after page load
        setTimeout(() => {
            // this.loader = false;
        }, 1500);
    }
}
