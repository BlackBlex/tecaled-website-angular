import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-contact-form',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.css'],
})
export class ContactFormComponent implements OnInit {
    form: FormGroup;

    status: string = '';

    constructor(private formBuilder: FormBuilder, private http: HttpClient) {
        this.form = this.formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            telephone: [
                '',
                [Validators.required, Validators.pattern('[0-9]{10}')],
            ],
            message: ['', [Validators.required, Validators.maxLength(500)]],
            recaptcha: [null, [Validators.required]],
        });
    }

    ngOnInit(): void {}

    save(event: Event) {
        event.preventDefault();

        if (this.form.valid) {
            const value = this.form.value;
            console.log(JSON.stringify(value));
            this.http
                .post('http://localhost/send_mail.php', JSON.stringify(value))
                .subscribe({
                    next: (v) => {
                        let jsonObj: any = JSON.parse(JSON.stringify(v));

                        if (jsonObj.status == 'success') {
                            this.status =
                                'Mensaje enviado, en breve nos comunicamos con usted';
                        } else {
                            this.status =
                                'Ha ocurrido un error al enviar el mensaje';
                        }
                    },
                });
        } else {
            this.form.markAllAsTouched();
        }
    }

    get f() {
        return this.form.controls;
    }
}
