import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from 'src/app/common/global-constants';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
    waUrl: string = GlobalConstants.waUrl;
    waNum: string = GlobalConstants.waNum;

    constructor() {}

    ngOnInit(): void {}
}
