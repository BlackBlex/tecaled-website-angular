import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-our-clients',
    templateUrl: './our-clients.component.html',
    styleUrls: ['./our-clients.component.css'],
})
export class OurClientsComponent implements OnInit {
    slides: Array<string> = [1, 2, 3, 4, 5].map(
        (n) => `assets/images/our-clients/${n}.png`
    );

    slideConfig = {
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 6,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                },
            },
        ],
    };

    constructor() {}

    ngOnInit(): void {}
}
