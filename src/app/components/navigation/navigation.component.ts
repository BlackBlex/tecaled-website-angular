import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {
    isMovil: string = 'undefined';

    navItems: Array<Array<String>> = [
        ['Inicio', '/home'],
        ['Servicios', '/services'],
        ['Casos de éxito', '/stories'],
        ['Distribuidores', '/distributor'],
        ['Contacto', '/contact'],
    ];

    constructor() {}

    ngOnInit(): void {}

    setMovil(): void {
        this.isMovil = 'collapse';
    }

    removeMovil(): void {
        this.isMovil = 'undefined';
    }
}
