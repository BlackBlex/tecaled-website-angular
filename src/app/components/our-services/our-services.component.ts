import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-our-services',
    templateUrl: './our-services.component.html',
    styleUrls: ['./our-services.component.css'],
    providers: [NgbCarouselConfig],
})
export class OurServicesComponent implements OnInit {
    images: Array<string> = [1, 2, 3, 4, 5].map(
        (n) => `assets/images/our-services/${n}.png`
    );
    texts: Array<Array<string>> = [
        [
            'Tecnología a tu alcance',
            '¡Te ofrecemos servicios terrestres y marínos!',
        ],
        [
            'Tecnología LTE',
            '¡Más posibilidad de comunicaciones con la nueva solución!',
        ],
        [
            'Soporte técnico',
            'Nuestro soporte técnico especializado está listo para resolver todas tus dudas, en línea o vía telefónica',
        ],
        [
            'Consultoría de redes y telecomunicaciones',
            '¡Buscaremos la solución integral más adecuada para optimizar la comunicación de tu arquitectura red industrial!',
        ],
        [
            'Radio Planning Network',
            '¡Te apoyamos en la planificación de tus futuros enlaces, enfocándonos a la alta disponibilidad del servicio, optimización y sin interferencias!',
        ],
    ];

    constructor(config: NgbCarouselConfig) {
        config.interval = 4000;
        config.wrap = true;
        config.keyboard = false;
        config.pauseOnHover = false;
        config.showNavigationArrows = false;
        config.showNavigationIndicators = false;
    }
    ngOnInit(): void {}
}
