import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navigation-services',
    templateUrl: './navigation-services.component.html',
    styleUrls: ['./navigation-services.component.css'],
})
export class NavigationServicesComponent implements OnInit {
    isMovil: string = 'undefined';

    navItems: Array<Array<String>> = [
        ['Consultoría de redes', 'consulting'],
        ['Radio planning network', 'radio-planning'],
        ['Satelital', 'satellite'],
        ['Tecnología LTE', 'lte'],
        ['Soporte técnico', 'technical-support'],
    ];

    constructor() {}

    ngOnInit(): void {}

    setMovil(): void {
        this.isMovil = 'collapse';
    }

    removeMovil(): void {
        this.isMovil = 'undefined';
    }
}
