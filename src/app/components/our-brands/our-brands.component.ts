import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-our-brands',
    templateUrl: './our-brands.component.html',
    styleUrls: ['./our-brands.component.css'],
})
export class OurBrandsComponent implements OnInit {
    slides: Array<string> = [1, 2, 3, 4, 5, 6, 7, 8].map(
        (n) => `assets/images/distributor/${n}.jpg`
    );

    slideConfig = {
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 6,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                },
            },
        ],
    };

    constructor() {}

    ngOnInit(): void {}
}
